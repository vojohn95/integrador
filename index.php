<!DOCTYPE HTML>
<html lang="es">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-T3268PN');</script>
<!-- End Google Tag Manager -->
<head>
    <meta charset="utf-8">
    <title>Integrador Technology</title>
    <meta name="description" content="Integrador es una empresa 100% mexicana que ofrece una amplia gama de productos y servicios de Soluciones de Tecnológica.">
    <meta content="Author" name="Luis Fernando Jonathan Vargas Osornio - johnvo@esquitechs.com">
    <meta name="keywords" content="tecnologia , desarrollo movil, desarrollo web, aplicaciones, java, php, android, .net">
    <meta name="revisit-after" content="14 days">
    <meta name="distribution" content="global">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta name= "robots" content="Index, Follow">
    <meta content="Integrador Technology" name="Integrador es una empresa 100% mexicana que ofrece una amplia gama de productos y servicios de Soluciones de Tecnológica.">
    <link href="img/favicon.icon.png" rel="icon">
    <!--<link rel="shortcut icon" type="image/x-icon" href="img/integrador.ico">-->
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="https://fonts.googleapis.com/css?family=Open Sans:300,300i,400,400i,700,700i|Poppins:300,400,500,700" rel="stylesheet">
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.icon.png">
    <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
    <link rel="canonical" href="http://integrador-technology.mx" />
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <link rel="canonical" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.2/css/bootstrap.min.css" />
    <link rel="canonical" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" async defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" async defer></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T3268PN"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header id="header" style="background-color:#737374">
    <div class="container" >

        <div id="logo" class="pull-left">
            <a href="#hero"><img src="img/logo.png" alt="Integrador Technology" title="Integrador technology" style="width:100%"/></img></a>
            <!-- Uncomment below if you prefer to use a text logo -->
            <!--<h1><a href="#hero">Regna</a></h1>-->
        </div>

        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li><a href="#hero" >INICIO</a></li>
                <li><a href="#about" >SOBRE</a></li>
                <li><a href="#services" >SERVICIOS</a></li>
                <li><a href="#portfolio" >PRODUCTOS Y ALIANZAS</a></li>
                <!--<li><a href="#team">EQUIPO</a></li>-->
                <!--<li class="menu-has-children"><a href="">MENU</a>
                  <ul>
                    <li><a href="#">Drop Down 1</a></li>
                    <li class="menu-has-children"><a href="#">Drop Down 2</a>
                      <ul>
                        <li><a href="#">Deep Drop Down 1</a></li>
                        <li><a href="#">Deep Drop Down 2</a></li>
                        <li><a href="#">Deep Drop Down 3</a></li>
                        <li><a href="#">Deep Drop Down 4</a></li>
                        <li><a href="#">Deep Drop Down 5</a></li>
                      </ul>
                    </li>
                    <li><a href="#">Drop Down 3</a></li>
                    <li><a href="#">Drop Down 4</a></li>
                    <li><a href="#">Drop Down 5</a></li>
                  </ul>
                </li>-->
                <li><a href="#contact" >Contacto</a></li>
                <!--<li><g:hangout render="createhangout"></g:hangout></li>-->
            </ul>
        </nav><!-- #nav-menu-container -->
    </div>
</header><!-- #header -->

<!--==========================
  Hero Section
============================-->
<section id="hero">
    <div class="hero-container">
        <h1>Somos Integrador Technology</h1>
        <h2>Disciplina - Productividad - Honestidad - Servicio - Compromiso - Lealtad - Liderazgo</h2>
        <a href="#about" class="btn-get-started">Conocenos</a>
    </div>
</section><!-- #hero -->

<main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
        <div class="container">
            <div class="row about-container">

                <div class="col-lg-6 content order-lg-1 order-2">
                    <h2 class="title" ALIGN="center">Sobre Nosotros</h2>
                    <p style="font-size:14px" ALIGN="justify">
                        Integrador es una empresa 100% mexicana que ofrece una amplia gama de productos y servicios de Soluciones de Tecnológica de Información de misión crítica, garantizando la implementación, administración y operación de los proyectos llave en mano de sus clientes.
                        Nuestras Soluciones Tecnológicas en Centros de datos, Hospedaje administrado, Call center, Mesa de ayuda, Consultoría, Equipamiento, Servicios en la Nube, Digitalización, Soluciones de Desarrollo de Aplicaciones móviles y en la Nube, soluciones de Seguridad Informática, Telecomunicaciones, Planes de Contingencia y Continuidad de Negocio, entre muchas otras, son ideales tanto para el sector público como el privado.
                    </p>
                    <br>
                    <div class="icon-box wow fadeInUp">
                        <div class="icon"><i class="fa fa-shopping-bag"></i></div>
                        <h4 class="title" ><a href="">Misión</a></h4>
                        <p class="description" ALIGN="justify">Ser un “INTEGRADOR estratégico” para nuestros clientes en Tecnologías de información, ofreciendo proyectos y servicios de manera eficiente, contribuyendo al logro de los objetivos del Negocio.</p>
                    </div>

                    <!--<div class="icon-box wow fadeInUp" data-wow-delay="0.2s">
                      <div class="icon"><i class="fa fa-photo"></i></div>
                      <h4 class="title"><a href="">Magni Dolores</a></h4>
                      <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                    </div>-->

                    <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
                        <div class="icon"><i class="fa fa-bar-chart"></i></div>
                        <h4 class="title"><a href="">Vision</a></h4>
                        <p class="description" ALIGN="justify">Posicionarnos como INTEGRADOR experto en tecnologías de información, integrando proyectos y servicios en soluciones totales, a la medida de las necesidades de nuestros clientes.</p>
                    </div>

                </div>

                <div class="col-lg-6 background order-lg-2 order-1 wow fadeInRight"></div>
            </div>

        </div>
    </section><!-- #about -->

    <!--==========================
      Facts Section experiencia
    ============================-->
    <!--<section id="facts">
      <div class="container wow fadeIn">
        <div class="section-header">
          <h3 class="section-title">Experiencia</h3>
          <p class="section-description">Contamos con distintos tipos de proyectos para todo tipo de clientes</p>
        </div>
        <div class="row counters">

  				<div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">150</span>
            <p>Clientes</p>
  				</div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">521</span>
            <p>Proyectos realizados</p>
  				</div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">10,463</span>
            <p>Horas trabajadas</p>
  				</div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">50</span>
            <p>Trabajadores experimentados</p>
  				</div>

  			</div>

      </div>
    </section>-->

    <!--==========================
      Services Section
    ============================-->
    <section id="services">
        <div class="container wow fadeIn">
            <div class="section-header">
                <h3 class="section-title">Servicios</h3>
                <br>
                <p class="section-description" ALIGN="justify">Integramos proyectos de Tecnologías de la Información, en Centros de Computo, Plataformas, Servidores, Sistemas Operativos, Bases de datos, Aplicaciones, Replicación, Almacenamiento, Manejadores de volúmenes, Monitoreo, Alta disponibilidad, Particiones, Virtualización, Clusterización, Cloud, Respaldos, Encriptación, Seguridad, Redes, Comunicaciones, Cableado, Atención a usuarios, programación, Oursourcing y Consultoría, reduciendo costos, garantizando la integridad, seguridad y confiabilidad de la información y asegurando la continuidad de las operaciones de los servicios proporcionados a nuestros clientes
                    .</p>
                <br>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="box">
                        <div class="icon"><a href=""><i class="fa fa-desktop"></i></a></div>
                        <h4 class="title"><a href="">Especialistas en administración de centros de computo y servicios en la nube</a></h4>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="box">
                        <div class="icon"><a href=""><i class="fa fa-bar-chart"></i></a></div>
                        <h4 class="title"><a href="">Desarrollamos, implementamos y administramos proyectos de infraestructura llave en mano</a></h4>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                    <div class="box">
                        <div class="icon"><a href=""><i class="fa fa-paper-plane"></i></a></div>
                        <h4 class="title"><a href="">Implementamos soluciones de desarrollo de aplicaciones moviles y en la nube</a></h4>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="box">
                        <div class="icon"><a href=""><i class="fa fa-photo"></i></a></div>
                        <h4 class="title"><a href="">data center's and cloud services</a></h4>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="box">
                        <div class="icon"><a href=""><i class="fa fa-road"></i></a></div>
                        <h4 class="title"><a href="">integramos soluciones de tecnologias de la información</a></h4>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                    <div class="box">
                        <div class="icon"><a href=""><i class="fa fa-shopping-bag"></i></a></div>
                        <h4 class="title"><a href="">consultoria especializada en tecnologia informatica</a></h4>
                    </div>
                </div>

            </div>




        </div>
    </section><!-- #services -->



    <section id="services">
        <div class="container wow fadeIn">
            <div class="section-header">
                <h3 class="section-title">Soporte</h3>
                <br>
                <p class="section-description" ALIGN="justify">Contamos con personal altamente capacitado y certificado en los diferentes productos de Hardware y Software que manejamos en nuestro portafolio, Implementamos proyectos con experiencia en la evaluación, análisis, dimensionamiento, implementación, instalación, consolidación, actualización, operación, configuración, migración, optimización, mantenimiento(preventivo y correctivo), soporte técnico, puesta a punto, tunning, aplicación de parches, assesment, servidores de aplicaciones, hardenings y vulnerabilidades.
                    Contamos con personal capacitado bajo los estandares que marca el PMI , ideales para llevar el control por completo de sus proyectos , no importa el nivel o volúmen que sea, logrando el éxito del mismo, una pronta mejora en sus procesos , controles de calidad y mejoramiento de su producción y ambiente en su empresa, obteniendo mejores ingresos, resultados precisos y reducción de costos en sus inversiones .</p>

                <ul class="section-description" >
                    <li ALIGN="justify">Soporte técnico especializado en cualquier situación de falla física y lógica de unidades de procesamiento, almacenamiento y comunicaciones.</li>
                    <li ALIGN="justify">Remplazo de partes o piezas dañadas en base a los niveles de servicios.</li>
                    <li ALIGN="justify">Servicios de administración de toda la infraestructura del software y hardware de su Centro de Computo o le brindamos arrendamiento del mismo para cualquiera de las marcas o líneas de productos de nuestro portafolio.</li>
                    <li ALIGN="justify">Servicios de Cloud IT para llevar su infraestructura a la nube, nube pública o privada, ofrecemos un traje a la medida de sus necesidades.</li>
                    <li ALIGN="justify">Esquema de atención 7X24 y 5X8, con atención telefónica, e-mail, vía remota y en sitio.</li>
                    <li ALIGN="justify">Servicio de mantenimiento preventivo y correctivo a infraestructura de hardware y software, que van desde 2 hrs tiempo de respuesta y 4 hrs tiempo de solución.</li>
                </ul>

                <section class="awSlider">
                    <div  class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target=".carousel" data-slide-to="0" class="active"></li>
                            <li data-target=".carousel" data-slide-to="1"></li>
                            <li data-target=".carousel" data-slide-to="2"></li>
                            <li data-target=".carousel" data-slide-to="3"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="https://www.periconsolutions.com/wp-content/uploads/2017/08/soporte-tecnico-empresarial.jpg" style="width: 700px; height: 340px;";>
                                <div class="carousel-caption">Soporte tecnico especializado en cualquier situación de falla física y lógica</div>
                            </div>
                            <div class="item">
                                <img src="https://ingenieriasamat.es/wp-content/uploads/2019/02/SOFTWARE.jpg" style="width: 700px; height: 340px;";>
                                <div class="carousel-caption">
                                    Servicios de administración de toda la infraestructura del software y hardware.
                                </div>
                            </div>
                            <div class="item">
                                <img src="http://www.incubarcolombia.org.co/media/zoo/images/cloud_service2_aaffe6a3de4eee4dc3201b1e6eda713c.jpg" style="width: 700px; height: 340px;";>
                                <div class="carousel-caption">
                                    Servicios de Cloud para llevar su infraestructura a la nube.
                                </div>
                            </div>
                            <div class="item">
                                <img src="https://3sesenta.mx/wordpress/wp-content/uploads/2015/09/Corbis-42-15107208.jpg" style="width: 700px; height: 340px;";>
                                <div class="carousel-caption">
                                    Servicios de mantenimiento correctivo y preventivo a infraestructura de hardware y software.</div>
                            </div>
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Geri</span>
                        </a>
                        <a class="right carousel-control" href="" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">İleri</span>
                        </a>
                    </div>
                </section>
                <!-- script y style para carrousel -->
                <script>
                    $('section.awSlider .carousel').carousel({
                        pause: "hover",
                        interval: 2000
                    });

                    var startImage = $('section.awSlider .item.active > img').attr('src');
                    $('section.awSlider').append('<img src="' + startImage + '">');

                    $('section.awSlider .carousel').on('slid.bs.carousel', function () {
                        var bscn = $(this).find('.item.active > img').attr('src');
                        $('section.awSlider > img').attr('src',bscn);
                    });

                </script>
                <style>
                    section.awSlider .carousel{
                        display:table;
                        z-index:2;
                        -moz-box-shadow: 0 0 4px #444;
                        -webkit-box-shadow: 0 0 4px #444;
                        box-shadow: 0 0 15px rgba(1,1,1,.5);
                    }

                    section.awSlider{
                        margin:30px auto;
                        padding:30px;
                        position:relative;
                        display:table;
                        -webkit-touch-callout: none;
                        -webkit-user-select: none;
                        -khtml-user-select: none;
                        -moz-user-select: none;
                        -ms-user-select: none;
                        user-select: none;
                    }

                    section.awSlider:hover > img{
                        -ms-transform: scale(0.0);
                        -webkit-transform: scale(0.0);
                        transform: scale(0.0);
                        opacity:1;
                    }

                    section.awSlider img{
                        pointer-events: none;
                    }

                    section.awSlider > img{
                        position:absolute;
                        top:30px;
                        z-index:1;
                        transition:all .3s;
                        filter: blur(1.8vw);
                        -webkit-filter: blur(2vw);
                        -moz-filter: blur(2vw);
                        -o-filter: blur(2vw);
                        -ms-filter: blur(2vw);
                        -ms-transform: scale(0.0);
                        -webkit-transform: scale(0.0);
                        transform: scale(0.0);
                        opacity:.5;
                    }
                </style>
                <!-- termina -->
            </div>

        </div>
    </section><!-- #services -->

    <!--==========================
    Call To Action Section
    ============================-->
    <section id="call-to-action">
        <div class="container wow fadeIn">
            <div class="row">
                <div class="col-lg-9 text-center text-lg-left">
                    <h3 class="cta-title">¡Comunicate!</h3>
                    <p class="cta-text"> Damos una atención rapida.</p>
                </div>
                <div class="col-lg-3 cta-btn-container text-center">
                    <a class="cta-btn align-middle" href="#contact">Pulse aqui</a>
                </div>
            </div>

        </div>
    </section><!-- #call-to-action -->

    <!--==========================
      Portfolio Section
    ============================-->
    <section id="portfolio">
        <div class="container wow fadeInUp">
            <div class="section-header">
                <h3 class="section-title">Productos y alianzas</h3>
                <br>
                <!--<p class="section-description">Pulse en cualquier boton para conocer</p>-->
            </div>
            <div class="row">

                <div class="col-lg-12">
                    <ul id="portfolio-flters">
                        <!--<li data-filter=".filter-app, .filter-card, .filter-logo, .filter-web" class="filter-active">Todo</li>
                        <li data-filter=".filter-app">Bases de datos</li>
                        <li data-filter=".filter-card">Redes e INFRAESTRUCTURA</li>-->
                        <li data-filter=".filter-logo">Productos</li>
                        <li data-filter=".filter-web">Alianzas</li>
                    </ul>
                </div>
            </div>

            <div class="row" id="portfolio-wrapper">
                <div class="col-lg-3 col-md-6 portfolio-item filter-logo filter-web">
                    <a>
                        <img src="img/portfolio/app1.jpg" alt="Oracle" title="oracle" style="width:120%; height:100%">
                        <div class="details">
                            <h4>Oracle</h4>
                            <span>Bases de Datos, Aplicaciones, Middleware, Servers y Storage Systems.</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-web" style="width:200px; height:200px">
                    <a>
                        <img src="img/portfolio/web2.jpg" alt="Avaya" title="avaya">
                        <div class="details">
                            <h4>Avaya</h4>
                            <span>Equipos de Telecomunicaciones</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-logo filter-web">
                    <a>
                        <img src="img/portfolio/app3.jpg" alt="Cisco" title="cisco" style="width:120%;">
                        <div class="details">
                            <h4>Cisco</h4>
                            <span>Routers, Switches, Wireless y Comunicaciones.</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-web">
                    <a>
                        <img src="img/portfolio/card1.jpg" alt="IBM" title="ibm">
                        <div class="details">
                            <h4>IBM</h4>
                            <span>Software, Hardware, Consultoría</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-logo">
                    <a>
                        <img src="img/portfolio/card2.jpeg" alt="Brocade" title="brocade" style="width:120%; height:100%">
                        <div class="details">
                            <h4>Brocade</h4>
                            <span>Routers, Switches, Wireless y Comunicaciones.</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-web">
                    <a>
                        <img src="img/portfolio/web3.jpg" alt="Rittal" title="rittal" style="width:120%; height:100%">
                        <div class="details">
                            <h4>Rittal</h4>
                            <span>Racks TI, Energía TI, Refrigeración TI, Monitorización TI, Soluciones de seguridad TI.</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-logo filter-web">
                    <a>
                        <img src="img/portfolio/card3.jpg" alt="Symantec" title="symantec" style="width:110%; height:100%">
                        <div class="details">
                            <h4>Symantec</h4>
                            <span>Protección de Datos, Alta disponibilidad, Archiving y Seguridad</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-logo filter-web">
                    <a>
                        <img src="img/portfolio/logo1.jpg" alt="Imperva" title="imperva" style="width:110%; height:100%">
                        <div class="details">
                            <h4>Imperva</h4>
                            <span>ADatabase Security, Web y File Security.</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-logo filter-web">
                    <a>
                        <img src="img/portfolio/app2.jpg" alt="Almacenamiento, Storage Systems, SAN y NAS Storage, Respaldos y Recuperación." title="net app" style="width:100%; padding-left:60px">
                        <div class="details">
                            <h4>Net App</h4>
                            <span>Almacenamiento, Storage Systems, SAN y NAS Storage, Respaldos y Recuperación.</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-web filter-web">
                    <a>
                        <img src="img/portfolio/logo2.jpg" alt="Citrix" title="citrix" style="width:105%; height:100%; padding-left:50px" >
                        <div class="details">
                            <h4>Citrix</h4>
                            <span> tecnologías de virtualización de servidores, conexión en red, software-como-servicio e informática en la nube</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-logo filter-web">
                    <a>
                        <img src="img/portfolio/web1.jpg" alt="F5" title="f5" style="width:110%; height:100%">
                        <div class="details">
                            <h4>F5</h4>
                            <span>Database Security, Web y File Security.</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-web">
                    <a>
                        <img src="img/portfolio/logo3.jpg" alt="DELL" title="dell" style="width:100%; height:100%; padding-left:50px">
                        <div class="details">
                            <h4>DELL</h4>
                            <span>computadoras personales, servidores, switches de red</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-logo filter-web">
                    <a>
                        <img src="img/portfolio/logow.png" alt="Vmware" title="vmware" style="width:110%; height:100%">
                        <div class="details">
                            <h4>Vmware</h4>
                            <span>Virtualization y Desktop Virtualization.</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-web">
                    <a>
                        <img src="img/portfolio/getac.jpg" alt="Getac" title="getac" style="width:110%; height:100%">
                        <div class="details">
                            <h4>Getac</h4>
                            <span>protección contra caídas, choques, derrames y vibración.</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-logo">
                    <a>
                        <img src="img/portfolio/logom.jpg" alt="Microsoft" title="microsoft" style="width:110%; height:130%">
                        <div class="details">
                            <h4>Microsoft</h4>
                            <span>Sistema Operativo y Herramientas de Escritorio.</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-web">
                    <a>
                        <img src="img/portfolio/hitachi.jpg" alt="Hitachi" title="hitachi" style="width:110%; height:130%">
                        <div class="details">
                            <h4>Hitachi</h4>
                            <span>Electrónica, Maquinaria industrial, Telecomunicaciones, Sistemas de información, Equipo de construcción, Defensa, Infraestructura ferroviaria</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-logo filter-web">
                    <a>
                        <img src="img/portfolio/logor.jpg" alt="Redhat" title="redhat" style="width:100%; height:100%">
                        <div class="details">
                            <h4>RedHat</h4>
                            <span>Sistema Operativo y Herramientas.</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-web">
                    <a>
                        <img src="img/portfolio/open.jpg" alt="OpenStack" title="openstack" style="width:100%; height:100%; padding-left:30px">
                        <div class="details">
                            <h4>Open Stack</h4>
                            <span>computación en la nube para proporcionar una infraestructura como servicio</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-web">
                    <a>
                        <img src="img/portfolio/sap.png" alt="SAP" title="sap" style="width:100%; height:100%; padding-left:30px">
                        <div class="details">
                            <h4>SAP</h4>
                            <span>software de planificación de recursos empresariales desarrollado por la compañía alemana SAP SE</span>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-6 portfolio-item filter-web">
                    <a>
                        <img src="img/portfolio/net.jpg" alt="Netscoutx" title="netscoutx" style="width:100%; height:100%; padding-left:30px">
                        <div class="details">
                            <h4>NETSCOUT</h4>
                            <span>productos de gestión de rendimiento de aplicaciones y redes.</span>
                        </div>
                    </a>
                </div>

            </div>

        </div>
    </section><!-- #portfolio -->

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact">
        <div class="container wow fadeInUp">
            <div class="section-header">
                <h3 class="section-title">Contacto</h3>
                <p class="section-description"></p>
            </div>
        </div>

        <!-- Uncomment below if you wan to use dynamic maps -->
        <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22864.11283411948!2d-73.96468908098944!3d40.630720240038435!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew York%2C NY%2C USA!5e0!3m2!1sen!2sbg!4v1540447494452" width="100%" height="380" frameborder="0" style="border:0" allowfullscreen></iframe>-->
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3764.407082642676!2d-99.20345148469505!3d19.351518386930547!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d2001082ccd883%3A0x73b3f38fbe0bc25!2sBlvd. Adolfo L%C3%B3pez Mateos 2349%2C Atlamaya%2C 01760 Ciudad de M%C3%A9xico%2C CDMX!5e0!3m2!1ses-419!2smx!4v1522813950315" width="100%" height="380" frameborder="0" style="border:0" allowfullscreen> </iframe>
        <div class="container wow fadeInUp mt-5">
            <div class="row justify-content-center">

                <div class="col-lg-3 col-md-4">

                    <div class="info">
                        <div>
                            <i class="fa fa-map-marker" style="padding-left:10px"></i>
                            <p ALIGN="justify">Blvd. Adolfo López Mateos 2349
                                Atlamaya, 01760 Ciudad de México, CDMX</p>
                        </div>

                        <div>
                            <i class="fa fa-diamond" aria-hidden="true"></i>
                            <p ALIGN="justify">Servicio de soporte a sus Ordenes.</p>
                            <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                            <p ALIGN="justify">Director Comercial</p>
                            <p ALIGN="justify"> Lic. Fernando Lara</p>
                            <i class="fa fa-envelope"></i>
                            <p ALIGN="justify">flara@integrador-technology.mx</p>
                            <div>
                                <i class="fa fa-phone"></i>
                                <p>Oficinas: +52 (55) 8525 3485</p>
                            </div>
                            <i class="fa fa-mobile" aria-hidden="true" style="padding-left:8px"></i>
                            <p>Cel: 52 (55) 4367 3019</p>
                        </div>
                    </div>

                    <div class="social-links" style="padding-left:50px">
                        <!--<a class="twitter" href=""><i class="fa fa-twitter"></i></a>
                        <a class="facebook" href="https://web.facebook.com/Integrador-Technology-1905285699600540/"><i class="fa fa-facebook"></i></a>
                        <a class="instagram" href="https://www.instagram.com/integrador_technology/"><i class="fa fa-instagram"></i></a>
                        <a class="linkedin" href=""><i class="fa fa-linkedin"></i></a>-->
                        <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                        <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                        <a class="instagram" href="#"><i class="fa fa-instagram"></i></a>
                        <a class="linkedin" href="https://mx.linkedin.com/in/integrador-technology-7b31a9124"><i class="fa fa-linkedin"></i></a>
                    </div>


                </div>

                <div class="col-lg-5 col-md-8">
                    <div class="form">
                        <div id="sendmessage">Mensaje enviado!, gracias por contactarnos!</div>
                        <div id="errormessage"></div>
                        <form action="correo.php" method="post" role="form">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" id="name" placeholder="Nombre" data-rule="minlen:4" data-msg="Porfavor ingrese minimo 4 letras" required/>
                                <div class="validation"></div>
                            </div>
                            <br>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Porfavor, ingrese un email valido" required/>
                                <div class="validation"></div>
                            </div>
                            <br>
                            <div class="form-group">
                                <input type="text" class="form-control" name="subject" id="subject" placeholder="Asunto" data-rule="minlen:4" data-msg="Minimo 8 caracteres de sujeto" required/>
                                <div class="validation"></div>
                            </div>
                            <br>
                            <div class="form-group">
                                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Por favor, escriba algo" placeholder="Escriba su mensaje" required></textarea>
                            </div>
                            <br>
                            <div class="text-center"><button type="submit">Enviar</button></div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- #contact -->

</main>

<!--==========================
  Footer
============================-->
<footer id="footer">
    <div class="footer-top">
        <div class="container">

        </div>
    </div>

    <div class="container">
        <div class="copyright">
            &copy; Copyright <strong>Integrador Technology</strong>. Todos los derechos reservados.
        </div>
    </div>
</footer><!-- #footer -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

<!-- JavaScript Libraries -->
<script src="lib/jquery/jquery.min.js"></script>
<script src="lib/jquery/jquery-migrate.min.js"></script>
<script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="lib/easing/easing.min.js"></script>
<script src="lib/wow/wow.min.js"></script>
<script src="lib/waypoints/waypoints.min.js"></script>
<script src="lib/counterup/counterup.min.js"></script>
<script src="lib/superfish/hoverIntent.js"></script>
<script src="lib/superfish/superfish.min.js"></script>

<!-- Contact Form JavaScript File -->
<script src="contactform/contactform.js"></script>

<!-- Template Main Javascript File -->
<script src="js/main.js"></script>

</body>
</html>
